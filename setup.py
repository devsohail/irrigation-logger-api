import os
from setuptools import setup, find_packages

current_directory = os.path.abspath(os.path.dirname(__file__))


with open(os.path.join(current_directory, 'README.md'), 'r') as f:
    README = f.read()


requires = [
    'alembic',
    'itsdangerous',
    'marshmallow',
    'marshmallow-sqlalchemy',
    'oauth2client',
    'passlib',
    'psycopg2',
    'pymysql',
    'pyramid >= 1.8.3',
    'pyramid_debugtoolbar',
    'pyramid_jinja2',
    'pyramid_tm',
    'sqlalchemy',
    'waitress',
    'webtest',
    'zope.sqlalchemy'
]

tests_require = [
    'WebTest >= 1.3.1',
    'pytest',
    'selenium'
]

setup(
    name='waterapp',
    version='0.0.1',
    description='REST client for irrigation logs',
    long_description='\n\n{}'.format(README),
    classifiers=[
        'Programming Language :: Python',
        'Framework :: Pyramid'
    ],
    author='Agbinder',
    author_email='dev@agbinder.com',
    url='agbinder.com',
    packages=find_packages(),
    include_package_data=True,
    zip_safe=False,
    extras_require={
        'testing': tests_require
    },
    install_requires=requires,
    entry_points={
        'paste.app_factory': [
            'main=waterapp:main'
        ],
        'console_scripts': [
            'app_init_db=waterapp.util.seed_data:initializedb'
        ]
    }
)
