# Orchard Water Log

This is a component of a larger orchard management system. This component takes care of generating 
irrigation reports.  Triggers will be able to tap endpoints to start and stop irrigation events.  

## Architecture
This project uses Pyramid and SQLAlchemy along with other supporting libraries

## Created By
Sohail Khan
