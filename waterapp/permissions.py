"""List all the permissions used in views and security"""

CreateOrchards = 'create_orchards'
ViewOwnOrchards = 'view_own_orchards'
ViewUser = 'view_user'
