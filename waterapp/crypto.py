import os

from itsdangerous import TimedJSONWebSignatureSerializer as Serializer, SignatureExpired, BadSignature
from passlib.context import CryptContext


def get_seconds(seconds=0, days=0, hours=0, minutes=0):
    return days * 24 * 60 * 60 + hours * 60 * 60 + minutes * 60 + seconds

EXPIRATION_TIME = {'days': 30}

user_pwd_context = CryptContext()
signer = None

def configure_passlib(settings):
    global user_pwd_context
    user_pwd_context.load(settings)


def from_module(passlib_ini, **kwargs):
    with open(passlib_ini, 'r') as fp:
        string = fp.read()
        configure_passlib(string)
    setup_signer(expires_in=kwargs.pop('expires_in', None))


def setup_signer(secret='bananas', expires_in=None):
    global signer
    expires_in = get_seconds(**EXPIRATION_TIME) \
        if expires_in is None else get_seconds(**expires_in)
    signer = Serializer(secret, expires_in=expires_in)


def includeme(config):
    global EXPIRATION_TIME, signer
    ini_file = os.path.join(os.path.dirname(config.basepath), 'passlib.ini')
    with open(ini_file, 'r') as fp:
        configure_passlib(fp.read())
    app_secret = config.registry.settings.get('app_secret', 'bananas')
    setup_signer(app_secret)


def sign(data):
    return signer.dumps(data)

def unsign(token):
    try:
        data = signer.loads(token)
    except BadSignature:
        return None
    except SignatureExpired:
        return None
    else:
        return data
