"""
    Creates the routes for the app
"""
from collections import namedtuple
from pyramid.security import Allow

from waterapp.models import User, Orchard, IrrigationEvent


route_t = namedtuple('Route', 'name pattern factory')

routes = (
    route_t('google_oauth', '/meta/oauth/google', ''),
    route_t('google_oauth_cb', '/meta/oauth/google-cb', ''),
    route_t('home', '/', ''),
    route_t('irrigation_event', '/irrigation-events/{irrigation_event_id}',
            'waterapp.resources.irrigation_resource_factory'),
    route_t('irrigation_events', '/orchards/{orchard_id}/irrigation-events',
            'waterapp.resources.orchard_factory'),
    route_t('login', '/authentication/tokens', ''),
    route_t('orchard', '/orchards/{orchard_id}',
            'waterapp.resources.orchard_factory'),
    route_t('orchards', '/orchards', 'waterapp.resources.user_resource_factory'),
    route_t('process_server_auth_code', '/meta/oauth/server-auth-code', ''),
    route_t('user', '/users/{user_id}', 'waterapp.resources.user_resource_factory'),
    route_t('user_orchards', '/users/{user_id}/orchards', 'waterapp.resources.user_resource_factory'),
    route_t('users', '/users', 'waterapp.resources.AdminResource'),
)


def includeme(config):
    for route in routes:
        if route.factory:
            config.add_route(route.name, route.pattern, factory=route.factory)
        else:
            config.add_route(route.name, route.pattern)
