import os
import sys
import json
from datetime import datetime, timedelta

from pyramid.paster import get_appsettings
from alembic import command
from alembic.config import Config

from waterapp import get_app_secrets
import waterapp.crypto
import waterapp.models.meta as meta
import waterapp.models as models


def read_data(json_file):
    data = {}
    with open(json_file, 'r') as fp:
        data = json.load(fp)

    return data

def seed_data(session, data):
    """ Adds sample data from the data dictionary.  Each orchard gets similar irrigation events"""
    existing_users = [t[0] for t in session.query(models.user.User.username).all()]
    for user in data.get('users', {}):
        if user.get('username') in existing_users:
            continue
        new_user = models.user.User(username=user.get('username'),
                                    email=user.get('email'),
                                    password='password')
        orchards = user.get('orchards')
        for orchard in orchards:
            new_orchard = models.orchard.Orchard(name=orchard.get('name'),
                                                 acres=orchard.get('acres'),
                                                 irrigation_type=orchard.get('irrigation_type'))
            new_orchard.lat = orchard['location']['lat']
            new_orchard.lon = orchard['location']['lon']

            for i in range(1,6):
                start_time = datetime(2016, i, 1)
                end_time = start_time + timedelta(minutes=15 + 15 * i + len(user['username']))
                new_irrigation_event = models.irrigation.IrrigationEvent(start_time=start_time,
                                                                         end_time=end_time)
                new_orchard.irrigation_events.append(new_irrigation_event)
            new_user.orchards.append(new_orchard)
        session.add(new_user)
    session.commit()


def upgrade_to_latest_version(ini_file_path):
    alembic_config = Config(ini_file_path)
    command.upgrade(alembic_config, 'head')

def reset_tables(ini_file_path):
    alembic_config = Config(ini_file_path)
    command.downgrade(alembic_config, 'base')
    upgrade_to_latest_version(ini_file_path)

def initializedb(ini_file='development.ini'):
    ini_file = ini_file
    settings = get_appsettings(ini_file, name='main')
    here = settings.get('here')
    if len(sys.argv) > 1:
        for arg in set(sys.argv[1:]):
            {
                'reset': reset_tables,
                'upgrade': upgrade_to_latest_version
            }.get(arg)(os.path.join(here, 'alembic.ini'))

    json_file = os.path.join(here, 'resources/sample_data.json')
    waterapp.crypto.from_module(os.path.join(here,'passlib.ini'))
    secrets = get_app_secrets(os.path.join(here, 'secrets.ini'))
    admin_username = secrets['admin_username']
    admin_password = secrets['admin_password']
    admin_email = secrets['admin_email']

    engine = meta.get_engine(settings)
    session_maker = meta.get_sessionmaker(settings, zte=False)
    session = session_maker()

    print("creating admin user:\nusername: {}\npassword: {}".format(admin_username, admin_password))
    admin = models.User(username=admin_username, email=admin_email, password=admin_password)
    session.add(admin)
    session.commit()

    print('reading json file')
    data = read_data(json_file)
    print('writing data')
    seed_data(session, data)

if __name__ == '__main__':
    initializedb()
