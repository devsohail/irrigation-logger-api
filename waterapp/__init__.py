import os

from pyramid.config import Configurator
from configparser import ConfigParser
from pyramid.authentication import BasicAuthAuthenticationPolicy


def get_app_secrets(ini_file="secrets.ini"):
    cfg = ConfigParser()
    cfg.read(ini_file)
    g_secret = cfg.get('secrets', 'google_secret')
    g_cid = cfg.get('secrets', 'google_client')
    app_secret = cfg.get('secrets', 'app_secret')
    admin_username = cfg.get('secrets', 'admin_username')
    admin_password = cfg.get('secrets', 'admin_password')
    admin_email = cfg.get('secrets', 'admin_email')
    return {'google_secret': g_secret,
            'google_client_id': g_cid,
            'app_secret': app_secret,
            'admin_username': admin_username,
            'admin_password': admin_password,
            'admin_email': admin_email
            }


def main(global_config, **app_settings):
    """ main configurator
        :return wsgi application
    """
    settings = global_config.copy()
    settings.update(app_settings)
    secrets = get_app_secrets(os.path.join(settings.get('here'), 'secrets.ini'))
    settings.update(secrets)

    config = Configurator(settings=settings)
    config.include('.security')
    config.include('pyramid_jinja2')
    config.add_static_view(name='static', path='waterapp:static')
    config.include('.cors')
    config.include('.crypto')
    config.include('.routes')
    config.scan('.views')
    config.include('.models')
    return config.make_wsgi_app()
