from pyramid.authentication import BasicAuthAuthenticationPolicy, extract_http_basic_credentials
from pyramid.authorization import ACLAuthorizationPolicy
from pyramid.security import (
    Authenticated,
    Everyone
)

from waterapp.models import User
from waterapp.crypto import unsign


class WaterappAuthnPolicy(BasicAuthAuthenticationPolicy):

    def authenticated_userid(self, request):
        username, password = extract_http_basic_credentials(request)
        user = check_user(username, password, request)
        if user:
            return user.id


class WaterappAuthzPolicy(ACLAuthorizationPolicy):
    pass


def get_user(request):
    username = request.unauthenticated_userid

    if username is not None:
        token_user = User.get_user_by_token(username, request.db)
        user = request.db.query(User).filter_by(username=username).first()
        return token_user if token_user else user


def check_user(username, password, request) -> User:
    using_token = username and password == ''
    if not using_token and password:
        user = request.db.query(User).filter_by(username=username).first()
    elif using_token:
        user = User.get_user_by_token(username, request.db)
    else:
        user = None
    if user and (using_token or user.verify_password(password)):
        return user


def principals_check(username, password, request) -> [str]:
    user = check_user(username, password, request)
    if user:
        principals = [str(user.id)]
        principals.append(user.username)    # TODO: to replace BasicAuthAuthenticationPolicy with own to avoid rep id
        if user.id == 1:  # admin user
            principals.append('admin')
        return principals


def includeme(config):
    config.set_authentication_policy(WaterappAuthnPolicy(check=principals_check))
    config.set_authorization_policy(WaterappAuthzPolicy())
    config.add_request_method(get_user, 'user', reify=True)
