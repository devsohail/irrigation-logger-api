""" Construct Resources From Objects """


from pyramid.security import Allow, ALL_PERMISSIONS, DENY_ALL, Authenticated
from pyramid.httpexceptions import HTTPNotFound
from waterapp.models import User, Orchard, IrrigationEvent
from waterapp.permissions import *


def irrigation_resource_factory(request):
    irrigation_event_id = request.matchdict['irrigation_event_id']
    irrigation_event = request.db.query(IrrigationEvent).filter_by(id=irrigation_event_id).first()
    if irrigation_event:
        return IrrigationEventResource(irrigation_event)
    return HTTPNotFound

def orchard_factory(request):
    orchard_id = request.matchdict['orchard_id']
    orchard = request.db.query(Orchard).filter_by(id=orchard_id).first()
    if orchard:
        return OrchardResource(orchard)
    return HTTPNotFound

def user_resource_factory(request):
    user = request.user
    if user:
        return UserResource(user)
    return HTTPNotFound

class UserResource:
    def __init__(self, user):
        self.user = user

    def __acl__(self):
        return [
            (Allow, Authenticated, (CreateOrchards)),
            (Allow, self.user.username, (ViewOwnOrchards, ViewUser)),
            (Allow, 'admin', ALL_PERMISSIONS),
            DENY_ALL
    ]

class OrchardResource:

    def __init__(self, orchard):
        self.orchard = orchard

    @property
    def __acl__(self):
        return [
            (Allow, 'admin', ALL_PERMISSIONS),
            (Allow, str(self.orchard.user_id), ALL_PERMISSIONS),
            DENY_ALL
        ]


class IrrigationEventResource:

    def __init__(self, irrigation_event):
        self.irrigation_event = irrigation_event

    @property
    def __acl__(self):
        return [
            (Allow, 'admin', ALL_PERMISSIONS),
            (Allow, str(self.irrigation_event.orchard.user_id), ALL_PERMISSIONS),
            DENY_ALL
        ]

class AdminResource:

    def __init__(self, request):
        pass

    __acl__ = [
        (Allow, 'admin', ALL_PERMISSIONS)
    ]
