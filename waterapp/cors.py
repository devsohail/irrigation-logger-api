from pyramid.security import NO_PERMISSION_REQUIRED

# https://www.html5rocks.com/static/images/cors_server_flowchart.png
# https://gist.github.com/mmerickel/1afaf64154b335b596e4


def includeme(config):
    settings = config.registry.settings
    cors_opts = get_cors_response_headers(settings)
    config.add_directive(
        'add_cors_preflight_handler', add_cors_preflight_handler)
    config.add_route_predicate('cors_preflight', CorsPreflightPredicate)

    config.add_subscriber(make_cors_response_modifier(cors_opts), 'pyramid.events.NewResponse')


def get_cors_response_headers(settings):
    cors_opts = {}
    for k in [i for i in settings.keys() if i.startswith('Access-Control')]:
        opt = settings.get(k)
        if ',' in opt:
            cors_opts[k] = (el.strip() for el in opt.split(','))
        else:
            cors_opts[k] = opt.strip()
    return cors_opts


def add_cors_preflight_handler(config):
    """used to add a preflight handler"""
    cors_opts = get_cors_response_headers(config.registry.settings)
    config.add_route(
        'cors-options-preflight', '/{catch_all:.*}',
        cors_preflight=True,        # this is what enables the CorsPreflightPredicate
    )

    # you can modify the behavior with permission
    config.add_view(
        make_cors_options_view(cors_opts),
        route_name='cors-options-preflight',
        permission=NO_PERMISSION_REQUIRED,
    )


def make_cors_options_view(cors_opts):
    """makes the cors_options_view with specified config options"""

    def cors_options_view(context, request):
        response = request.response
        if 'Access-Control-Request-Headers' in request.headers:
            response.headers['Access-Control-Allow-Methods'] = ",".join(cors_opts.get('Access-Control-Allow-Methods'))
        response.headers['Access-Control-Allow-Headers'] = ",".join(cors_opts.get('Access-Control-Allow-Headers'))

        if cors_opts.get('preflight_cache_delay', False):
            response.headers['Access-Control-Max-Age'] = str(
                int(cors_opts.get('preflight_cache_delay')) * 24 * 60 * 60)

        response['Access-Control-Allow-Origin'] = cors_opts.get('Access-Control-Allow-Origin')
        return response

    return cors_options_view


class CorsPreflightPredicate:
    """Checks to see if the request is a preflight request"""
    def __init__(self, val, config):
        self.val = val

    def text(self):
        return 'cors_preflight = {}'.format(bool(self.val))

    phash = text

    def __call__(self, info, request):
        if not self.val:
            return False
        is_options_request = request.method == 'OPTIONS'
        origin_in_header = 'Origin' in request.headers
        access_control_in_header = 'Access-Control-Request-Method' in request.headers
        is_preflight = is_options_request and access_control_in_header and origin_in_header
        return is_preflight


def make_cors_response_modifier(cors_opts):
    def add_cors_to_response(event):
        """Modify the outgoing response by setting the response headers for a successful CORS request"""
        request = event.request
        response = event.response

        response.headers['Access-Control-Expose-Headers'] = ",".join(cors_opts.get('Access-Control-Expose-Headers'))
        response.headers['Access-Control-Allow-Origin'] = cors_opts.get('Access-Control-Allow-Origin')
        response.headers['Access-Control-Allow-Credentials'] = (cors_opts
                                                                .get('Access-Control-Allow-Credentials', 'false'))
    return add_cors_to_response
