from logging import getLogger

from pyramid.httpexceptions import HTTPBadRequest, HTTPNoContent
from pyramid.view import view_config, view_defaults
from sqlalchemy.orm.exc import NoResultFound

from ..models import Orchard, User
from ..models.schema import OrchardSchema
from ..permissions import *


@view_defaults(renderer='json')
class OrchardViews:

    def __init__(self, request):
        self.request = request
        self.logger = getLogger(__name__)

    @view_config(route_name='user_orchards', request_method='GET')
    def get_user_orchards(self):

        try:
            user = self.request.db.query(User).filter_by(username=self.request.matchdict['user_id']).one()
            orchards = user.orchards
            data = OrchardSchema().dump(orchards, many=True).data
        except NoResultFound as exc:
            self.request.response.status = 400
            return dict(errors=['resource does not exist'])
        return dict(orchards=data)

    @view_config(route_name='user_orchards', request_method='POST', permission=CreateOrchards)
    @view_config(route_name='orchards', request_method='POST', permission=CreateOrchards)
    def create_new_orchard(self):
        jb = self.request.json_body
        user = self.request.user
        try:
            data, errors = OrchardSchema().load(jb, session=self.request.db)
            if errors:
                err = HTTPBadRequest
                err.errors = errors
                raise err
            user.orchards.append(data)
            self.request.db.flush()
        except HTTPBadRequest as exc:
            if hasattr(exc, 'errors'):
                errors = exc.errors
            else:
                errors = ['improperly formatted request']
            self.request.response.status = exc.code
            return dict(errors=errors)
        else:
            self.request.response.status = 201
            return {'message': 'success', 'data': OrchardSchema().dump(data).data}

    @view_config(route_name='orchard', request_method='GET', permission='view')
    def get_orchard(self):
        orchard = self.request.context.orchard
        obj = OrchardSchema().dump(orchard)
        return obj.data

    @view_config(route_name='orchard', request_method='PATCH', permission='edit')
    def edit_orchard_info(self):
        errors = {}
        orchard = self.request.context.orchard
        try:
            # example of patching, load directly into an existing row
            obj, errors = OrchardSchema(partial=True, exclude=['id']) \
                .load(self.request.json_body, session=self.request.db, instance=orchard)
            if errors:
                err = HTTPBadRequest()
                err.errors = errors
                raise err
        except HTTPBadRequest as exc:
            errors = getattr(exc, 'errors', ['invalid request'])
            self.request.response.status = exc.code
            return {"errors": errors}
        except ValueError as exc:
            self.request.response.status = 400
            return {"errors": errors}
        else:
            return HTTPNoContent()

    @view_config(route_name='orchard', request_method='PUT', permission='edit')
    def replace_orchard_info(self):
        errors = {}
        orchard = self.request.context.orchard
        try:
            obj, errors = OrchardSchema(exclude=['id'], partial=False) \
                .load(self.request.json_body, session=self.request.db, instance=orchard)
            if errors:
                err =  HTTPBadRequest()
                err.errors = errors
        except HTTPBadRequest as exc:
            errors = getattr(exc, 'errors', ['invalid request'])
            self.request.response.status = exc.code
            return {"errors": errors}
        except ValueError as exc:
            self.request.response.status = HTTPBadRequest.code
            return {'errors': errors}
        else:
            return OrchardSchema().dump(orchard).data

    @view_config(route_name='orchard', request_method='DELETE', permission='delete')
    def delete_orchard(self):
        orchard = self.request.context.orchard
        self.request.db.delete(orchard)
        return {'message': 'deleted'}

