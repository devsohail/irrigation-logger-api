from logging import getLogger

from pyramid.view import view_config, view_defaults
from pyramid.httpexceptions import HTTPUnauthorized
from sqlalchemy.orm.exc import NoResultFound

from waterapp.models import Orchard, IrrigationEvent
from waterapp.models.schema import IrrigationEventSchema


@view_defaults(renderer='json')
class IrrigationEventViews:
    def __init__(self, request):
        self.request = request
        self.logger = getLogger(__name__)
        self.matchdict = request.matchdict
        self.json_body = request.json_body if request.body else {}
        self.db = self.request.db

    @view_config(route_name='irrigation_event', request_method='GET', permission='view')
    def get_one_irrigation_event(self):
        event = self.request.context.irrigation_event
        dump = IrrigationEventSchema().dump(event, many=False)
        return dump.data

    @view_config(route_name='irrigation_events', request_method='GET', permission='view')
    def get_irrigation_events(self):
        orchard = self.request.context.orchard
        irrigation_events = orchard.irrigation_events
        dump = IrrigationEventSchema().dump(irrigation_events, many=True)
        return {'irrigationEvents': dump.data}

    @view_config(route_name='irrigation_events', request_method='POST', permission='edit')
    def start_watering_event(self):
        orchard = self.request.context.orchard
        try:
            irrigation_event, errors = IrrigationEventSchema().load(self.json_body, session=self.db)
            if errors:
                raise ValueError()
        except ValueError:
            self.request.response.status = 400
            return {"errors": errors}
        else:
            orchard.irrigation_events.append(irrigation_event)
            self.db.flush()
            return IrrigationEventSchema().dump(irrigation_event).data

    @view_config(route_name='irrigation_event', request_method='PATCH', permission='edit')
    def edit_irrigation_event(self):
        """
        This endpoint can be used to modify an irrigation event.  Specifically, you can use this to set an
         updated end_time
        :return: data dictionary
        """
        event = self.request.context.irrigation_event
        irrigation_event, errors = IrrigationEventSchema(partial=True) \
            .load(self.json_body, session=self.db, instance=event)
        return IrrigationEventSchema().dump(irrigation_event).data

    @view_config(route_name='irrigation_event', request_method='DELETE', permission='edit')
    def delete_irrigation_event(self):
        try:
            event = self.db.query(IrrigationEvent).filter_by(id=self.matchdict['irrigation_event_id']).one()
        except NoResultFound as exc:
            self.request.response.status = 404
            return {'error': 'no object exists'}
        else:
            self.db.delete(event)
            return {'message': 'deleted event'}
