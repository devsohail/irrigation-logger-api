import json
from os import path
from urllib.request import Request, urlopen
import urllib.parse

from webob.exc import HTTPBadRequest, HTTPForbidden
from pyramid.view import view_config
from oauth2client.client import flow_from_clientsecrets

from waterapp.models import User
from waterapp.models.schema import UserSchema

base_directory = path.dirname(path.dirname(path.dirname(__file__)))

redirect_endpoint = '/meta/oauth/google-cb'
flow = flow_from_clientsecrets(path.join(base_directory, 'client_secret_807476606136.json'),
                               scope='https://www.googleapis.com/auth/plus.login',
                               redirect_uri='http://127.0.0.1:6543' + redirect_endpoint)

@view_config(route_name="google_oauth", request_method="GET")
def google_oauth(request):
    """ initialize oauth2 request """
    ...


@view_config(route_name='process_server_auth_code', request_method='POST', renderer='json')
def process_server_auth_code(request):
    """ receives and processes the server auth code
        body should contain the parameters: email, name, serverAuthCode, and optionally, scopes
    """
    data = request.json_body
    server_auth_code = data.get('serverAuthCode')
    email = data.get('email')
    user = request.db.query(User).filter_by(username=email).one_or_none()
    if user:
        # user exists already, get their refresh token and try to validate
        refresh_token = user.oauth_refresh_token
        r = Request('https://www.googleapis.com/oauth2/v4/token', method='POST')
        data = urllib.parse.urlencode({
            'client_id': flow.client_id,
            'client_secret': flow.client_secret,
            'refresh_token': refresh_token,
            'grant_type': 'refresh_token'
        })

        with urlopen(r, data.encode('ascii')) as req:
            rv = json.loads(req.read().decode('utf-8'))
            if not rv['access_token']:
                raise HTTPForbidden('not authorized to retrieve data')
        user.oauth_access_token = rv['access_token']
        user_data = UserSchema().dump(user).data
        return dict(token=user.generate_client_side_code().decode('ascii'), message='success', **user_data)
    else:
        try:
            credentials = flow.step2_exchange(server_auth_code)
            new_user = User.from_oauth(email, credentials.access_token, credentials.refresh_token)
        except Exception as e:
            return HTTPBadRequest('missing server auth code: {}'.format(str(e)))
        else:
            request.db.add(new_user)
            request.db.flush()
            return dict(token=new_user.generate_client_side_code().decode('ascii'), message='success')


@view_config(route_name="google_oauth_cb", request_method="POST")
def google_callback(request):
    """ google needs to callback to this service to send tokens """
    # get the oauth auth token in the request

    # exchange auth token for access token
    print(request)
