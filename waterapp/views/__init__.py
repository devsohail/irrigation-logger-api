from pyramid.view import view_config
from pyramid.httpexceptions import HTTPUnauthorized
from pyramid.security import forget
from pyramid.view import forbidden_view_config

@forbidden_view_config()
def forbidden_challenge_view(request):
    response = HTTPUnauthorized()
    response.headers.update(forget(request))
    return response

@view_config(route_name='home', renderer='waterapp:templates/home.jinja2', accept='text/html')
def main(request):
    return {}

@view_config(route_name='home', renderer='json', accept='application/json')
def main_json(request):
    message = '''this is the api for the irrigation app. Use the given url
    anchors as templates
    '''
    return {
        'message': message,
        'login': 'auth/token',
        'oauthCallback': 'meta/oauth/server-auth-code',
        'userInfoTemplate': 'users/{username}',
        'userOrchardsTemplate': 'users/{username}/orchards',
        'userOrchardTemplate': 'orchards/{orchard_id}',
        'orchardIrrigationEvents': 'orchards/{orchard_id}/irrigation-events'
    }
