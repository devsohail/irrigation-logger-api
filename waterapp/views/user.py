from logging import getLogger

from pyramid.view import view_config, view_defaults
from sqlalchemy.orm.exc import NoResultFound
from pyramid.httpexceptions import HTTPException, HTTPUnauthorized
from pyramid.authentication import extract_http_basic_credentials

from waterapp.models import User
from waterapp.models.schema import UserSchema
from waterapp.security import check_user
from waterapp.permissions import ViewUser


@view_defaults(renderer='json')
class UserViews:

    def __init__(self, request):
        self.request = request
        self.logger = getLogger(__name__)

    @view_config(route_name='users', request_method='GET', permission='view')
    def get_all_users(self):
        all_users = self.request.db.query(User).all()
        return {'users': UserSchema(exclude=['password_hash'], many=True).dump(all_users).data}

    @view_config(route_name='users', request_method='POST')
    def register(self):
        try:
            data, errors = UserSchema().load(self.request.json_body, session=self.request.db)
            if errors:
                exc = ValueError()
                exc.errors = errors
                raise exc
        except ValueError as exc:
            self.request.response.status = 400
            return {'success': False,
                    'message': 'you supplied incorrect information',
                    'errors': exc.errors if hasattr(exc, 'errors') else {}}
        else:
            self.request.db.add(data)
            return {'success': True, 'message': "login by getting an authentication token"}

    @view_config(route_name='user', request_method='GET', permission=ViewUser)
    def get_user(self):
        username = self.request.matchdict['user_id']
        try:
            user = self.request.db.query(User).filter_by(username=username).one()
        except NoResultFound:
            self.request.response.status = 404
            return {'error': 'resource does not exist'}
        else:
            return UserSchema().dump(user).data

@view_defaults(renderer='json')
class UserAuthViews:

    def __init__(self, request):
        self.request = request
        self.logger = getLogger(__name__)

    @view_config(route_name='login')
    def login(self):
        """gets username and password from request and logs user-in"""
        try:
            credentials = extract_http_basic_credentials(self.request)

            if credentials is None:
                raise HTTPUnauthorized()
            username, password = credentials
            user = check_user(username, password, self.request)
            if not user:
                raise HTTPUnauthorized
            token = user.generate_client_side_code()
        except HTTPUnauthorized as e:
            self.request.response.status = e.status_int
            return {'error': e.explanation}
        else:
            return {'token': token.decode('ascii') }
