""" Include all the models in here and set initialize the session request hooks"""


from . import meta
from .irrigation import IrrigationEvent
from .orchard import Orchard
from .user import User


def includeme(config):
    settings = config.get_settings()

    config.include('pyramid_tm')
    sessionmaker = meta.get_sessionmaker(settings)

    config.registry['dbsession'] = sessionmaker
    config.add_request_method(lambda request: sessionmaker(), 'db', reify=True)
