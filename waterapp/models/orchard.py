from sqlalchemy import (
    Column,
    ForeignKey,
    Integer,
    Float,
    Unicode
)
from sqlalchemy.orm import relationship

from .meta import Base

class Orchard(Base):
    """
        Model that represents an orchard
    """
    user_id = Column(Integer, ForeignKey('user.id'))
    name = Column(Unicode(128), nullable=False)
    lat = Column(Float())
    lon = Column(Float())
    acres = Column(Float())
    irrigation_type = Column(Unicode(256))

    # related properties
    irrigation_events = relationship('IrrigationEvent', back_populates='orchard')
    user = relationship('User', back_populates='orchards')

    def __repr__(self):
        return "<Orchard: name={name}>".format(name=self.name)
