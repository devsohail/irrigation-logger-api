import re

from sqlalchemy import Column, Integer, engine_from_config
from sqlalchemy.ext.declarative import declarative_base, declared_attr
from sqlalchemy.orm import sessionmaker
from sqlalchemy.schema import MetaData
from zope.sqlalchemy import ZopeTransactionExtension


convention = {
    'pk': 'pk_%(table_name)s',
    'ix': 'ix_%(column_0_label)s',
    'uq': 'uq_%(table_name)s_%(column_0_label)s',
    'ck': 'ck_%(table_name)s_%(constraint_name)s',
    'fk': 'fk_%(table_name)s_%(column_0_name)s_%(referred_table_name)s'
}


def get_engine(settings):
    engine = engine_from_config(settings, 'sqlalchemy.')
    return engine


def get_sessionmaker(settings, zte=True):
    engine = get_engine(settings)
    extensions = []
    if zte:
        extensions.append(ZopeTransactionExtension())
    maker = sessionmaker(extension=extensions)
    maker.configure(bind=engine)
    return maker

metadata = MetaData(naming_convention=convention)


class Base:

    @declared_attr
    def __tablename__(cls):
        """Convert ClassName to class_name for table name"""
        class_name_tokens = re.findall('([A-Z][a-z]*)', cls.__name__)
        return '_'.join([token.lower() for token in class_name_tokens])

    id = Column(Integer, primary_key=True)


Base = declarative_base(cls=Base, metadata=metadata)
