from sqlalchemy import (
    Column,
    DateTime,
    Integer,
    ForeignKey
)
from sqlalchemy.orm import relationship

from .meta import Base


class IrrigationEvent(Base):

    orchard_id = Column(Integer, ForeignKey('orchard.id'), nullable=False)
    orchard = relationship('Orchard', back_populates='irrigation_events')
    start_time = Column(DateTime(), nullable=False)
    end_time = Column(DateTime())

    def __repr__(self):
        return "<IrrigationEvent start_time={} end_time={}>".format(self.start_time, self.end_time)
