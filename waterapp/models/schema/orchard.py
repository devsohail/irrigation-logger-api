from marshmallow import Schema, fields
from marshmallow_sqlalchemy import ModelSchema

from ..orchard import Orchard

class OrchardSchema(ModelSchema):

    lat = fields.Float(load_from="latitude", dump_to="latitude")
    lon = fields.Float(load_from="latitude", dump_to="longitude")

    class Meta:
        model = Orchard