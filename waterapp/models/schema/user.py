from marshmallow import Schema, fields
from marshmallow_sqlalchemy import ModelSchema


from ..user import User
from .orchard import OrchardSchema

class UserSchema(ModelSchema):

    password_hash = fields.String(load_from='password')
    orchards = fields.Nested(OrchardSchema, only=('id', 'name'), many=True)

    class Meta:
        model = User
        exclude = ('oauth_access_token', 'oauth_refresh_token', 'password_hash')