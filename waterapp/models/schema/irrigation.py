from marshmallow import Schema, fields
from marshmallow_sqlalchemy import ModelSchema

from ..irrigation import IrrigationEvent


class IrrigationEventSchema(ModelSchema):
    start_time = fields.DateTime(required=True, load_from='startTime', dump_to='startTime')
    end_time = fields.DateTime(load_from='endTime', dump_to='endTime')

    class Meta:
        model = IrrigationEvent
