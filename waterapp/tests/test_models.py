import time
from sqlalchemy.exc import IntegrityError
from waterapp.tests.test_base import TransactionalTests


class UserModelTests(TransactionalTests):

    @classmethod
    def setUpClass(cls):
        super().setUpClass()

        # setup crypto
        from waterapp.crypto import from_module
        from_module('passlib.ini')

    def setUp(self):
        super().setUp()

    def test_database_should_be_empty(self):
        from waterapp.models import User
        self.assertEqual(0, len(self.session.query(User).all()))

    def test_two_users_with_same_email_cannot_exist(self):
        from waterapp.models import User
        u1 = User(username='username', password='pass')
        u2 = User(username='username', password='pass')

        with self.assertRaises(IntegrityError):
            self.session.add(u1)
            self.session.add(u2)
            self.session.commit()

    def test_can_get_user_with_signed_token(self):
        from waterapp.models import User

        u = User(username='username', email='username@email.com', password='pass')
        self.session.add(u)
        self.session.commit()
        token = u.generate_client_side_code()

        u2 = User.get_user_by_token(token, self.session)

        self.assertEqual(u, u2)

    def test_token_contains_timestamp(self):
        from waterapp.models import User
        from waterapp.crypto import unsign

        u = User(username='username', email='username@email.com', password='pass')
        self.session.add(u)
        self.session.commit()
        token = unsign(u.generate_client_side_code())

        self.assertIn('timestamp', token)

    def test_tokens_contain_different_timestamps(self):
        from waterapp.models import User
        from waterapp.crypto import unsign

        u = User(username='username', email='username@email.com', password='pass')
        self.session.add(u)
        self.session.commit()

        first_token = unsign(u.generate_client_side_code())
        second_token = unsign(u.generate_client_side_code())

        self.assertLess(first_token['timestamp'], second_token['timestamp'])


    def test_can_get_id_if_no_session_is_provided_with_token_unsigning(self):
        from waterapp.models import User
        u = User(username='username', email='username@email.com', password='pass')
        self.session.add(u)
        self.session.commit()
        token = u.generate_client_side_code()

        data = User.get_user_by_token(token)

        self.assertEqual(data['user_id'], u.id)

    def test_failed_token_gives_none(self):
        from waterapp.models import User
        u = User(username='username', email='username@email.com', password='pass')
        self.session.add(u)
        self.session.commit()
        token = u.generate_client_side_code()
        token += b'X'

        data = User.get_user_by_token(token)

        self.assertIsNone(data)

    def test_expired_token_gives_none(self):
        from waterapp.models import User
        from waterapp.crypto import from_module
        from_module('passlib.ini', expires_in={'seconds': 0})
        u = User(username='username', email='username@email.com', password='pass')
        self.session.add(u)
        self.session.commit()
        token = u.generate_client_side_code()

        time.sleep(1)
        data = User.get_user_by_token(token)

        self.assertIsNone(data)

    def test_user_dumps_without_sensitive_info(self):
        from waterapp.models.schema import UserSchema
        from waterapp.models import User
        from waterapp.crypto import from_module
        from_module('passlib.ini', expires_in={'seconds': 60})
        u = User(username='username', email='username@email.com', password='pass')
        self.session.add(u)
        self.session.commit()

        user_dump = UserSchema().dump(u)

        self.assertNotIn('password_hash', user_dump.data)
        self.assertNotIn('oauth_access_token', user_dump.data)
        self.assertNotIn('oauth_refresh_token', user_dump.data)

class IrrigationEventModelTests(TransactionalTests):

    def test_irrigation_event_is_correctly_serialized(self):
        """ test if irrigation event has been serialized correctly """
        self.fail("needs implementation")
