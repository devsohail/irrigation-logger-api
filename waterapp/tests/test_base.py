""" Base Test that Test Cases Inherit """
import unittest
import os

from alembic import command
from alembic.config import Config
from pyramid import testing
from pyramid.paster import get_appsettings
from sqlalchemy import create_engine


DEFAULT_DB = 'postgres'


class BaseTest(unittest.TestCase):
    """
        Performs initialization of tests
        see http://docs.pylonsproject.org/projects/pyramid/en/latest/narr/testing.html#test-setup-and-teardown
    """

    def setUp(self):
        self.config = testing.setUp()

    def tearDown(self):
        testing.tearDown()


def get_db_url_components(url):
    return url.rsplit('/', 1)


def setup_test_database(url):
    base_url, database_name = get_db_url_components(url)
    engine = create_engine(base_url + '/' + DEFAULT_DB)
    conn = None
    try:
        conn = engine.connect()
        q = conn.execute('SELECT * FROM pg_database WHERE datname = \'{}\''.format(database_name))
        if len(q.fetchall()):
            return
        conn.execute('COMMIT')
        conn.execute('CREATE DATABASE {}'.format(database_name))
    except Exception as exc:
        print(exc)
        print("could not create database")
        raise exc
    finally:
        if conn and not conn.closed:
            conn.close()


def drop_database(url):
    base_url, database_name = get_db_url_components(url)
    engine = create_engine(base_url + '/' + DEFAULT_DB)
    conn = None
    try:
        conn = engine.connect()
        conn.execute('COMMIT')
        conn.execute("""
         SELECT pg_terminate_backend(pg_stat_activity.pid)
         FROM pg_stat_activity
         WHERE pg_stat_activity.datname = '{}'
        """.format(database_name))
        conn.execute('DROP DATABASE IF EXISTS {}'.format(database_name))
    except Exception as exc:
        print("could not remove database: {}\n{}".format(database_name, str(exc)))
    finally:
        if conn and not conn.closed:
            conn.close()


class DBMixin:

    @classmethod
    def setup_alembic(cls, settings):
        alembic_ini = 'alembic.ini'
        here = settings.get('here')
        alembic_config = Config(os.path.join(here, alembic_ini))
        alembic_config.set_main_option('sqlalchemy.url', settings.get('sqlalchemy.url'))
        cls.alembic_config = alembic_config

    @classmethod
    def setup_database(cls, settings):
        from waterapp.models.meta import get_sessionmaker
        setup_test_database(settings.get('sqlalchemy.url'))
        cls.session_maker = get_sessionmaker(settings, zte=False)

    @classmethod
    def destroy_database(cls):
        cls.session_maker.kw.get('bind').dispose()
        drop_database(cls.settings.get('sqlalchemy.url'))

    def init_db(self):
        command.upgrade(self.alembic_config, 'head')
        self.session = self.session_maker()

    def tear_down_db(self):
        self.session.close()
        command.downgrade(self.alembic_config, 'base')


class TransactionalTests(BaseTest, DBMixin):
    """
        Performs database tests
    """

    @classmethod
    def setUpClass(cls):
        """ Sets up the test database """
        ini_file = 'test.ini'
        cls.settings = settings = get_appsettings(ini_file, name='main')
        cls.setup_alembic(settings)
        cls.setup_database(settings)

    @classmethod
    def tearDownClass(cls):
        cls.destroy_database()

    def setUp(self):
        super().setUp()
        super().init_db()

    def test_smoke_db(self):
        self.assertEqual(self.session.execute("SELECT 1").first()[0], 1)

    def tearDown(self):
        super().tearDown()
        super().tear_down_db()


class FunctionalBase(unittest.TestCase, DBMixin):
    """
        For Functional Tests
    """

    @classmethod
    def setUpClass(cls):
        ini_file = 'test.ini'
        cls.settings = settings = get_appsettings(ini_file, name='main')
        cls.settings = get_appsettings(ini_file, name='main')
        cls.setup_alembic(settings)
        cls.setup_database(settings)

    def setUp(self):
        self.config = testing.setUp()
        from waterapp import main
        app = main(global_config={}, **self.settings)
        super().init_db()
        from webtest import TestApp
        self.testapp = TestApp(app)
        self.session = self.session_maker()

    @classmethod
    def tearDownClass(cls):
        cls.destroy_database()

    def tearDown(self):
        testing.tearDown()
        super().tear_down_db()

    @property
    def _basic_header(self):
        if hasattr(self, 'username') and hasattr(self, 'password'):
            return 'Basic', (self.username, self.password)
        return 'Basic', ('', '')
