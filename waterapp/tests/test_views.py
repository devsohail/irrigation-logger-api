import json
import time
from collections import OrderedDict
from datetime import datetime, timedelta

from waterapp.tests.test_base import BaseTest, FunctionalBase


def create_user(username, email, password, session=None):
    from waterapp.models import User
    try:
        u = User(username=username, email=email, password=password)
        session.add(u)
        session.commit()
    except Exception as e:
        print(e)
        raise e
    else:
        return u if u else None

def create_orchard(user, orchard_data, session=None):
    from waterapp.models import Orchard
    try:
        o = Orchard(**orchard_data)
        user.orchards.append(o)
        session.commit()
    except Exception as e:
        print(e)
        raise e
    else:
        return o if o else None


def create_irrigation_event(orchard, data={}, session=None):
    from waterapp.models import IrrigationEvent
    try:
        i = IrrigationEvent(**data)
        orchard.irrigation_events.append(i)
        session.commit()
    except Exception as e:
        print(e)
        raise e
    else:
        return i if i else None


def create_default_fixtures(instance, session, options=None):
    """

    :param instance:
    :param session:
    :param options:
    :return: tuple(User, Orchard, IrrigationEvent)
    """
    options = options if options else {}

    u = o = i = None
    if not options.get('no_user', False):
        u = create_user(instance.username, instance.email, instance.password, session)
    if not options.get('no_orchard', False):
        o = create_orchard(u, {'name': instance.orchard_name}, session)
    if not options.get('no_irrigation_event', False):
        i = create_irrigation_event(o, {'start_time': datetime.now()}, session)
    return u, o, i


class UserViewTests(BaseTest):


    def test_user_registration_view_invalid(self):
        from waterapp.views.user import UserViews
        self.fail("not implemented")

    def test_user_registration_view_valid(self):
        from waterapp.views.user import UserViews

        self.fail("not implemented")


class UserViewFunctionalTests(FunctionalBase):

    def test_main(self):
        res = self.testapp.get('/')
        self.assertIn(b'Orchard Irrigation Log', res.body)


class ApiTests(FunctionalBase):
    pass


class UserApiTests(ApiTests):
    username = "test"
    email = username + "@email.com"
    password = 'abcdefghijk'

    def test_users_endpoint(self):
        res = self.testapp.get('/users', expect_errors=True)
        self.assertEqual(res.status_int, 401)

        u = create_user(self.username, self.email, self.password, self.session)
        self.testapp.set_authorization(self._basic_header)
        res = self.testapp.get('/users')
        self.assertEqual(res.status_int, 200)

    def test_can_login_a_user(self):
        u = create_user('test', 'test@test.com', 'test', self.session)

        self.testapp.set_authorization(('Basic', ('test', 'test')))
        res = self.testapp.post('/authentication/tokens')

        self.assertIn('token', res.json)

    def test_login_doesnt_reuse_tokens(self):
        u = create_user('test', 'test@test.com', 'test', self.session)

        self.testapp.set_authorization(('Basic', ('test', 'test')))
        res = self.testapp.post('/authentication/tokens')
        first_token = res.json.get('token', 'something')

        res = self.testapp.post('/authentication/tokens')
        second_token = res.json.get('token', 'something')

        self.assertNotEqual(first_token, second_token)

    # app does not return the proper error code upon
    # exception
    def test_login_needs_authorization_headers(self):
        from webob.exc import HTTPUnauthorized
        u = create_user('test', 'test@test.com', 'test', self.session)

        res = self.testapp.post('/authentication/tokens', expect_errors=True)
        self.assertEqual(res.status_int, HTTPUnauthorized.code)

    def test_rejects_invalid_logins(self):
        u = create_user('test', 'test@test.com', 'test', self.session)

        self.testapp.set_authorization(('Basic', ('test', 'tes')))
        res = self.testapp.post('/authentication/tokens', expect_errors=True)

        self.assertEqual(401, res.status_int)

    def test_can_get_user_data_if_admin(self):
        admin = create_user('admin', 'admin@test.com', 'test', self.session)
        u = create_user(self.username, self.email, self.password, self.session)
        self.testapp.set_authorization(('Basic', ('admin', 'test')))

        res = self.testapp.get('/users/{0.username}'.format(u))

        self.assertEqual(res.status_int, 200)

    def test_can_get_user_data_if_logged_in_as_self(self):
        admin = create_user('admin', 'admin@test.com', 'test', self.session)
        u = create_user(self.username, self.email, self.password, self.session)
        self.testapp.set_authorization(self._basic_header)

        res = self.testapp.get('/users/{0.username}'.format(u))

        self.assertEqual(res.status_int, 200)


    def test_user_data_endpoint_also_loads_orchard_data(self):
        admin = create_user('admin', 'admin@test.com', 'test', self.session)
        u = create_user(self.username, self.email, self.password, self.session)
        self.testapp.set_authorization(self._basic_header)
        o = create_orchard(u, {'name': 'aname'}, self.session)

        res = self.testapp.get('/users/{0.username}'.format(u))

        self.assertEqual(len(res.json['orchards']), 1)
        self.assertIn(o.name, [_o['name'] for _o in res.json['orchards']])

class IrrigationEventApiTests(ApiTests):
    username = "test"
    email = username + "@email.com"
    password = 'abcdefghijk'
    orchard_name = 'test orchard'

    def test_get_an_irrigation_event(self):
        u, o, i = create_default_fixtures(self, self.session, {'no_irrigation_event': True})
        now = datetime.now()
        i = create_irrigation_event(o, {'start_time': now}, self.session)

        self.testapp.set_authorization(self._basic_header)
        res = self.testapp.get('/orchards/{0.id}/irrigation-events'.format(o))

        self.assertEqual(res.content_type,  'application/json')
        self.assertEqual(res.status_int, 200)
        self.assertIn(now.isoformat(), res.body.decode('utf-8'))
        self.assertIn('irrigationEvents', res.json)
        self.assertEqual(1, len(res.json.get('irrigationEvents', [])))


    def test_complete_an_irrigation_event(self):
        u = create_user(self.username, self.email, self.password, self.session)
        o = create_orchard(u, {'name': self.orchard_name}, self.session)
        now = datetime.now()
        end = now + timedelta(hours=1)
        i = create_irrigation_event(o, {'start_time': now}, self.session)

        # TODO: check why marshmallow loads datetime without microseconds in UnmarshalResult
        self.testapp.set_authorization(('Basic', (self.username, self.password)))
        res = self.testapp.patch_json('/irrigation-events/{0.id}'.format(o), {'endTime': end.isoformat()})
        ending_time = datetime.strptime(res.json.get('endTime'), '%Y-%m-%dT%H:%M:%S+00:00')
        diff_seconds = (end - ending_time).seconds

        self.assertEqual(res.content_type, 'application/json')
        self.assertEqual(res.status_int, 200)
        self.assertEqual(diff_seconds, 0)
        self.assertIn(now.isoformat(), res.body.decode('utf-8'))

    def test_start_an_irrigation_event_with_initial_time(self):
        u, o, i = create_default_fixtures(self, self.session, {'no_irrigation_event': True})
        self.testapp.set_authorization(self._basic_header)

        start_time = datetime.now()

        data = {
            'startTime': start_time.isoformat()
        }

        res = self.testapp.post_json('/orchards/{0.id}/irrigation-events'.format(o), data)
        json_res = res.json_body

        self.assertEqual(200, res.status_int)
        self.assertIsNone(json_res['endTime'])
        self.assertEqual(o.id, json_res['orchard'])


    def test_can_only_get_irrigation_event_if_user_owns_orchard(self):
        u2name, u2email, u2password = ('user2', 'user2@email.com', 'password')
        u = create_user(self.username, self.email, self.password, self.session)
        u2 = create_user(u2name, u2email, u2password, self.session)
        o = create_orchard(u, {'name': self.orchard_name}, self.session)
        now = datetime.now()
        i = create_irrigation_event(o, {'start_time': now}, self.session)

        self.testapp.set_authorization(('Basic', (u2name, u2password)))

        res = self.testapp.get('/orchards/{0.id}/irrigation-events'.format(o),
                               expect_errors=True)

        self.assertEqual(res.status_int, 401)

    def test_can_use_crypto_token_to_access_a_resource(self):
        # not the best place for this but it works for now
        u = create_user(self.username, self.email, self.password, self.session)
        o = create_orchard(u, {'name': self.orchard_name}, self.session)
        now = datetime.now()
        i = create_irrigation_event(o, {'start_time': now}, self.session)

        self.testapp.set_authorization(('Basic', (u.generate_client_side_code().decode('ascii'), '')))
        res = self.testapp.get('/orchards/{0.id}/irrigation-events'.format(o))

        self.assertEqual(200, res.status_int)


class OrchardApiTests(FunctionalBase):
    username = "test"
    email = username + "@email.com"
    password = 'abcdefghijk'

    def test_can_get_orchard(self):
        u = create_user(self.username, self.email, self.password, self.session)
        o = create_orchard(u, {'name': 'my_orchard'}, self.session)
        self.testapp.set_authorization(self._basic_header)

        res = self.testapp.get('/orchards/{0.id}'.format(o))

        self.assertEqual(res.json['name'], 'my_orchard')

    def test_only_owners_can_get_orchard(self):
        from webob.exc import HTTPUnauthorized
        u = create_user(self.username, self.email, self.password, self.session)
        o = create_orchard(u, {'name': 'my_orchard}'}, self.session)
        self.testapp.set_authorization(('Basic', ('test', 'test')))

        res = self.testapp.get('/orchards/{0.id}'.format(o), expect_errors=True)

        self.assertEqual(res.status_int, HTTPUnauthorized.code)

    def test_logged_in_user_can_create_new_orchards(self):
        u = create_user(self.username, self.email, self.password, self.session)
        self.testapp.set_authorization(self._basic_header)
        orchard_data = {
            'name': 'myorchard',
            'latitude': 32.31,
            'longitude': 121.32,
            'acres': 32,
            'irrigation_type': 'drip'
        }

        # first route
        res = self.testapp.post_json('/orchards', orchard_data)

        self.assertEqual(res.status_int, 201)
        self.assertEqual(res.json['data']['name'], orchard_data['name'])

        # second route
        res = self.testapp.post_json('/users/{0.username}/orchards'.format(u), orchard_data)

        self.assertEqual(res.status_int, 201)
        self.assertEqual(res.json['data']['name'], orchard_data['name'])

    def test_only_logged_in_users_can_create_orchards(self):
        from webob.exc import HTTPForbidden

        res = self.testapp.post('/orchards', {'name': 'a_name'}, expect_errors=True)
        self.assertTrue(res.status_int, HTTPForbidden.code)

        u = create_user(self.username, self.email, self.password, self.session)
        res = self.testapp.post('/users/1/orchards', {'name': 'a_name'}, expect_errors=True)
        self.assertTrue(res.status_int, HTTPForbidden.code)

    def test_non_owners_cannot_edit(self):
        from webob.exc import HTTPUnauthorized
        u = create_user(self.username, self.email, self.password, self.session)
        o = create_orchard(u, {'name': 'my_orchard', 'acres': 32.12}, self.session)
        u2 = create_user('test2', 'test2@email.com', 'pass', self.session)
        self.testapp.set_authorization(('Basic', ('test2', 'pass')))

        res = self.testapp.patch_json('/orchards/{0.id}'.format(o), {'name': 'test_name'}, expect_errors=True)
        self.assertEqual(res.status_int, HTTPUnauthorized.code)

    def test_owner_of_orchard_can_edit(self):
        from pyramid.httpexceptions import HTTPNoContent
        u = create_user(self.username, self.email, self.password, self.session)
        o = create_orchard(u, {'name': 'my_orchard', 'acres': 32.12}, self.session)
        self.testapp.set_authorization(self._basic_header)
        new_name = 'new_name'

        res = self.testapp.patch_json('/orchards/{0.id}'.format(o), {'name': new_name})
        self.assertEqual(res.status_int, HTTPNoContent.code)

    def test_owner_can_replace_orchard_info(self):
        u = create_user(self.username, self.email, self.password, self.session)
        o = create_orchard(u, {'name': 'my_orchard', 'acres': 32.12}, self.session)
        self.testapp.set_authorization(self._basic_header)
        new_name = 'new_name'

        res = self.testapp.put_json('/orchards/{0.id}'.format(o), {'name': new_name})

        self.assertEqual(res.status_int, 200)
        self.assertEqual(res.json['name'], new_name)

    def test_owner_can_delete_orchard(self):
        u = create_user(self.username, self.email, self.password, self.session)
        o = create_orchard(u, {'name': 'my_orchard', 'acres': 32.12}, self.session)
        self.testapp.set_authorization(self._basic_header)

        res = self.testapp.delete('/orchards/{0.id}'.format(o))
        self.assertEqual(res.status_int, 200)
        self.assertIn('deleted', res.json['message'])

        res = self.testapp.get('/orchards/{0.id}'.format(o), expect_errors=True)
        self.assertNotEqual(res.status_int, 200)

    def test_user_can_get_list_of_all_orchards(self):
        u = create_user(self.username, self.email, self.password, self.session)
        o = create_orchard(u, {'name': 'my_orchard', 'acres': 32.12}, self.session)
        o2 = create_orchard(u, {'name': 'anothername', 'acres': 12.34}, self.session)
        self.testapp.set_authorization(self._basic_header)

        res = self.testapp.get('/users/{0.username}/orchards'.format(u))
        orchards = res.json['orchards']

        self.assertIn('orchards', res.json)
        self.assertEqual(len(orchards), 2)
