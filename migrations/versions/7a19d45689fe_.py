"""add oauth signin column

Revision ID: 7a19d45689fe
Revises: 84b3e785f67c
Create Date: 2017-04-26 11:14:51.548723

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '7a19d45689fe'
down_revision = '84b3e785f67c'
branch_labels = None
depends_on = None

def upgrade():
    op.add_column('user', sa.Column('oauth_signin', sa.Boolean))

def downgrade():
    op.drop_column('user', 'oauth_signin')
