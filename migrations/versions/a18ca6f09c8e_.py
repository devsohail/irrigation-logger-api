"""adds oauth columns

Revision ID: a18ca6f09c8e
Revises: 7a19d45689fe
Create Date: 2017-04-26 11:36:31.954765

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'a18ca6f09c8e'
down_revision = '7a19d45689fe'
branch_labels = None
depends_on = None

def upgrade():
    op.add_column('user', sa.Column('oauth_access_token', sa.Unicode(length=255), nullable=True))
    op.add_column('user', sa.Column('oauth_refresh_token', sa.Unicode(length=255), nullable=True))


def downgrade():
    op.drop_column('user', 'oauth_access_token')
    op.drop_column('user', 'oauth_refresh_token')
